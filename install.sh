#!/bin/sh
export DEBIAN_FRONTEND=noninteractive;
echo "====================================";
echo "Installing ZSH";  
echo "====================================";

apt -y install zsh  && 
chsh -s /bin/zsh;  

if [ -f ./.zshrc ]
then
rm ./.zshrc;
echo "setopt autocd" > ./.zshrc; 
source ./.zshrc; 
fi

if [ -f $HOME/.zshrc ]
then
rm $HOME/.zshrc;
echo "setopt autocd" > $HOME/.zshrc;
source $HOME/.zshrc; 
fi

echo "====================================";
echo "Hello";
echo "This is Iggy Autorave and I'm about to install your webserver";  
echo "====================================";

GCP_INIT="$HOME/.tempscripts";
chmod u+x $GCP_INIT/*; 

echo "====================================";
echo "Setting Time";  
echo "====================================";

$GCP_INIT/setTime.sh; 

echo "====================================";
echo "Installing Vim latest Version";  
echo "====================================";

$GCP_INIT/installVim.sh; 

echo "====================================";
echo "Installing Git";  
echo "====================================";

$GCP_INIT/installGit.sh; 

echo "====================================";
echo "Installing CURL";  
echo "====================================";

apt -y install curl; 

echo "====================================";
echo "Installing NODE";  
echo "====================================";

$GCP_INIT/installNode.sh; 

echo "====================================";
echo "Installing Python3";  
echo "====================================";

$GCP_INIT/installPy.sh; 

echo "====================================";
echo "Installing Zinit";  
echo "====================================";

$GCP_INIT/installZinit.sh; 

echo "====================================";
echo "Let me configure SSH to a different Port"; 
echo "====================================";

$GCP_INIT/sshConfig.sh;

echo "====================================";
echo "Thank you, Now your Webserver is set and Ready"; 
echo "This is your ssh-key";
echo "====================================";
if [ -f $HOME/.ssh/id_rsa.pub ]
then
    cat $HOME/.ssh/id_rsa.pub; 
else
    echo "No Legacy"; 
fi

if [ -f $HOME/.ssh/id_ed25519.pub ]
then
    cat $HOME/.ssh/id_ed25519.pub; 
else
    echo "Modern"; 
fi

echo "====================================";
echo "Compleating Cycle";
sudo apt -y update && 
sudo apt -y clean && 
sudo apt -y autoremove && 
sudo rm -rf $GCP_INIT; 
echo "====================================";

echo "====================================";
echo "We are the sum of our memories. Erasing the memories edged in oneself is the same as loosing oneself.";
echo "Iggy will close, Thank you Nora.";
echo "====================================";
