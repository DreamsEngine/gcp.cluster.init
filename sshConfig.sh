cp /etc/ssh/sshd_config /etc/ssh/sshd_config.original && chmod a-w /etc/ssh/sshd_config.original; 

mkdir -p $HOME/temp && 
cd $HOME/temp && 
git clone https://gist.github.com/caf6269624d2253971334cc0c503e313.git ./ && 
mv sshd_config_gcp sshd_config && 
cp $HOME/temp/sshd_config /etc/ssh && 
chmod 664 /etc/ssh/sshd_config && 
cd $HOME && 
rm -rf $HOME/temp;

sudo systemctl restart sshd.service && 
service sshd restart; 

echo "====================================";
echo "Let's create SSH-KEY"; 
echo "====================================";
SERVERNAME=`hostname`;
ssh-keygen -t ed25519 -C  "$SERVERNAME"; 
eval "$(ssh-agent -s)";  