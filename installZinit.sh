sh -c "$(curl -fsSL https://git.io/zinit-install)"; 

echo "====================================";
echo "I will configure ZSHRC for you";  
echo "====================================";

if [ -f $HOME/.zshrc ]
then 
rm -rf $HOME/.zshrc && 
mkdir -p $HOME/temp &&  
cd $HOME/temp && 
git clone https://gist.github.com/f92d40a0e7804d51ba7b7f1d4ce1b3b3.git ./ && 
mv .zshrc-gcp .zshrc && 
cp .zshrc $HOME && 
cd $HOME && 
rm -rf $HOME/temp;
fi


echo "====================================";
echo "Zinit is almost done, next time you"; 
echo "return to this machine you will be"; 
echo "able to configure Zinit"; 
echo "====================================";
